// Package opennotify is a thingful fetcher that fetch data
// about the  International Space Station (ISS) position
package opennotify

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"github.com/thingful/thingfulx"
	"golang.org/x/net/context"
)

const (
	// ProviderWebpage is the upstream provider website homepage
	ProviderWebpage = "http://open-notify.org/"

	// BaseURL is is the url used for the http request
	BaseURL = "http://api.open-notify.org/iss-now.json"

	// Metadata is our additional metadata about the thing
	Metadata = "space,orbit,iss,science,nasa,esa,astronaut,cosmonaut,taikonaut,roscosmos,jaxa,csa,spacestation,thingful:network=opennotify"
)

// openNotifyData is a struct for parsing data from open-notify
type openNotifyData struct {
	Status    string      `json:"message"`
	Position  ISSPosition `json:"iss_position"`
	Timestamp int64       `json:"timestamp"`
}

// ISSPosition parses the current location of the ISS
type ISSPosition struct {
	Lat string `json:"latitude"`
	Lng string `json:"longitude"`
}

// NewFetcher instantiates a new Fetcher instance. All fetchers must try to
// load any required config values from namespaced environment variables.
func NewFetcher() (thingfulx.Fetcher, error) {
	u, _ := url.Parse(BaseURL)
	webpage, _ := url.Parse(ProviderWebpage)

	dProvider := &thingfulx.Provider{
		Name:        "Open Notify",
		UID:         "opennotify",
		Description: "This is a simple api to return the current location of the ISS. It returns the current latitude and longitude of the space station with a unix timestamp for the time the location was valid.",
		Webpage:     webpage,
	}

	return &fetcher{
		provider: dProvider,
		BaseURL:  u,
	}, nil
}

type fetcher struct {
	provider *thingfulx.Provider
	BaseURL  *url.URL
}

// Provider returns information about the upstream data provider.
func (f fetcher) Provider() *thingfulx.Provider {
	return f.provider
}

// URLS returns the minimum set of urls required to fully index this data provider.
func (f fetcher) URLS(ctx context.Context, client thingfulx.Client, delay time.Duration) ([]string, error) {
	return []string{f.BaseURL.String()}, nil
}

// Fetch a resource from the upstream provider if we can.
func (f fetcher) Fetch(ctx context.Context, url string, client thingfulx.Client, timeProvider thingfulx.TimeProvider) ([]thingfulx.Thing, error) {
	// create a new request object
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	// make the requests
	resp, err := client.DoHTTP(req)
	if err != nil {
		return nil, err
	}

	defer func() {
		if cerr := resp.Body.Close(); err == nil && cerr != nil {
			err = cerr
		}
	}()

	// ensure response status code is ok else return error
	if resp.StatusCode != http.StatusOK {
		if resp.StatusCode == http.StatusNotFound {
			return nil, thingfulx.ErrNotFound
		}
		return nil, thingfulx.NewErrUnexpectedResponse(resp.Status)
	}

	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return f.parse(bytes, timeProvider)
}

func (f *fetcher) parse(data []byte, timeProvider thingfulx.TimeProvider) ([]thingfulx.Thing, error) {

	parsed := openNotifyData{}

	err := json.Unmarshal(data, &parsed)

	if err != nil {
		return nil, err
	}

	things := []thingfulx.Thing{}

	lat, err := strconv.ParseFloat(parsed.Position.Lat, 64)
	if err != nil {
		return nil, err
	}

	long, err := strconv.ParseFloat(parsed.Position.Lng, 64)
	if err != nil {
		return nil, err
	}

	if parsed.Status == "success" {
		thing := thingfulx.Thing{
			Title:       "International Space Station Current Location",
			Description: "The International Space Station is moving at close to 28,000 km/h so its location changes really fast! Where is it right now?",
			Category:    &thingfulx.Miscellaneous,
			Webpage:     "http://open-notify.org/Open-Notify-API/ISS-Location-Now/",
			DataURL:     "http://api.open-notify.org/iss-now.json",
			IndexedAt:   timeProvider.Now(),
			Lng:         long,
			Lat:         lat,
			Provider:    f.provider,
			Visibility:  thingfulx.Open,
			Metadata:    Metadata,
			Channels:    nil,
			RawData:     data,
		}

		things = append(things, thing)

		return things, nil
	}

	return nil, thingfulx.ErrInvalidData
}
