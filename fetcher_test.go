package opennotify

import (
	"io/ioutil"
	"net/http"
	"net/url"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/thingful/httpmock"
	"github.com/thingful/thingfulx"
	"golang.org/x/net/context"
)

func TestNewFetcher(t *testing.T) {
	_, err := NewFetcher()
	assert.Nil(t, err)
}

func TestProvider(t *testing.T) {
	fetcher, _ := NewFetcher()

	webpage, _ := url.Parse("http://open-notify.org/")

	expected := &thingfulx.Provider{
		Name:        "Open Notify",
		UID:         "opennotify",
		Description: "This is a simple api to return the current location of the ISS. It returns the current latitude and longitude of the space station with a unix timestamp for the time the location was valid.",
		Webpage:     webpage,
	}

	dProvider := fetcher.Provider()

	assert.Equal(t, expected, dProvider)
}

func TestURLS(t *testing.T) {
	fetcher, _ := NewFetcher()
	client := thingfulx.NewClient("thingful", time.Duration(1)*time.Second)
	delay := time.Duration(0)

	URLsSlice, err := fetcher.URLS(context.Background(), client, delay) 
	if err != nil {
		t.Errorf("Error creating the fetcher URLS slice")
	}

	expected := []string{"http://api.open-notify.org/iss-now.json"}

	assert.Equal(t, expected, URLsSlice)
}

func TestFetchValid(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	fetcher, _ := NewFetcher()

	client := thingfulx.NewClient("thingful", time.Duration(1)*time.Second)

	timeProvider := thingfulx.NewMockTimeProvider(time.Now())

	rawData, err := ioutil.ReadFile("data/valid.json")
	assert.Nil(t, err)

	testcases := []struct {
		url            string
		respStatusCode int
		respBody       []byte
		expected       []thingfulx.Thing
	}{
		{
			url:            "http://api.open-notify.org/iss-now.json",
			respStatusCode: http.StatusOK,
			respBody:       rawData,
			expected: []thingfulx.Thing{
				thingfulx.Thing{
					Title:       "International Space Station Current Location",
					Description: "The International Space Station is moving at close to 28,000 km/h so its location changes really fast! Where is it right now?",
					Category:    &thingfulx.Miscellaneous,
					Webpage:     "http://open-notify.org/Open-Notify-API/ISS-Location-Now/",
					DataURL:     "http://api.open-notify.org/iss-now.json",
					IndexedAt:   timeProvider.Now(),
					Lng:         -42.007565603262265,
					Lat:         21.93138737771598,
					Provider:    fetcher.Provider(),
					Visibility:  thingfulx.Open,
					Metadata:    "space,orbit,iss,science,nasa,esa,astronaut,cosmonaut,taikonaut,roscosmos,jaxa,csa,spacestation,thingful:network=opennotify",
					Channels:    nil,
					RawData:     rawData,
				},
			},
		},
	}

	for _, testcase := range testcases {
		httpmock.Reset()
		httpmock.RegisterStubRequest(
			httpmock.NewStubRequest(
				"GET",
				testcase.url,
				httpmock.NewBytesResponder(testcase.respStatusCode, testcase.respBody),
			),
		)

		things, err := fetcher.Fetch(context.Background(), testcase.url, client, timeProvider)
		if assert.NoError(t, err) {
			assert.Equal(t, testcase.expected, things)
		}
	}
}

func TestFetchInvalid(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	fetcher, _ := NewFetcher()

	client := thingfulx.NewClient("thingful", time.Duration(1)*time.Second)

	timeProvider := thingfulx.NewMockTimeProvider(time.Now())

	testcases := []struct {
		url            string
		respStatusCode int
		filename       string
	}{
		{
			url:            "http://api.open-notify.org/iss-now.json",
			respStatusCode: http.StatusNotFound,
			filename:       "data/valid.json",
		},
		{
			url:            "http://api.open-notify.org/iss-now.json",
			respStatusCode: http.StatusOK,
			filename:       "data/invalid.json",
		},
		{
			url:            "http://api.open-notify.org/iss-now.json",
			respStatusCode: http.StatusOK,
			filename:       "data/failure.json",
		},
	}

	for _, testcase := range testcases {
		b, err := ioutil.ReadFile(testcase.filename)
		if assert.NoError(t, err) {

			httpmock.Reset()
			httpmock.RegisterStubRequest(
				httpmock.NewStubRequest(
					"GET",
					testcase.url,
					httpmock.NewBytesResponder(testcase.respStatusCode, b),
				),
			)

			_, err = fetcher.Fetch(context.Background(), testcase.url, client, timeProvider)
			assert.NotNil(t, err)
		}
	}
}

func TestFetchResponseError(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	fetcher, _ := NewFetcher()

	client := thingfulx.NewClient("thingful", time.Duration(1)*time.Second)

	timeProvider := thingfulx.NewMockTimeProvider(time.Now())

	testcases := []struct {
		url            string
		respStatusCode int
		expected       *thingfulx.ErrUnexpectedResponse
	}{
		{
			url:            "http://api.open-notify.org/iss-now.json",
			respStatusCode: http.StatusNotFound,
			expected:       thingfulx.ErrNotFound,
		},
		{
			url:            "http://api.open-notify.org/iss-now.json",
			respStatusCode: http.StatusRequestTimeout,
			expected:       thingfulx.NewErrUnexpectedResponse("408"),
		},
	}

	for _, testcase := range testcases {
		httpmock.Reset()
		httpmock.RegisterStubRequest(
			httpmock.NewStubRequest(
				"GET",
				testcase.url,
				httpmock.NewBytesResponder(testcase.respStatusCode, []byte{}),
			),
		)

		_, err := fetcher.Fetch(context.Background(), testcase.url, client, timeProvider)
		assert.Equal(t, err, testcase.expected)
	}
}

func TestFetchError(t *testing.T) {
	fetcher, _ := NewFetcher()

	client := thingfulx.NewClient("thingful", time.Duration(1)*time.Second)

	timeProvider := thingfulx.NewMockTimeProvider(time.Now())

	testcases := []string{"", "%"}

	for _, url := range testcases {
		_, err := fetcher.Fetch(context.Background(), url, client, timeProvider)
		assert.NotNil(t, err)
	}
}
